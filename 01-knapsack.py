#!/usr/bin/python3 -tt
'''------------------------------------- >Last Modified on Tue, 24 May 2016< '''
'''
Knapsack
https://www.hackerrank.com/challenges/unbounded-knapsack
Time complexity: O(nk)

This is a standard dynamic programming problem. A fundamental idea of dynamic 
programming can be found in the slides 
http://www.es.ele.tue.nl/education/5MC10/Solutions/knapsack.pdf .
(I learned this PDF in Editorial.)

I have solved the problem at first without dynamic programming. Because some 
test cases contain a lot of items whose weight exceeds the limit, (a 
degenerate version of) brute force works after removing such a useless item.

A rough problem description is following: we have items and list of their 
weights. We also have a knapsack. We want to put items in the knapsack as 
heavy as possible. Answer the maximum weight.

Here we note that we can use an item multiple times. So if there is an item 
of weight 1, then the answer is the same as the limit of the weight.

The following data are given. 

- A : list of weights
- n : length of list A
- k : expected sum (limit of the sum)

Let M[k] be the maximum we want to find. Then the identities

    M[0] = 0, and 
    M[x] = max ( { M[x-w]+w | w in A, w <= x } cup { M[x-1] } )

hold.
'''

for _ in range(int(input())):
    n,k = map(int,input().split())
    A = map(int,input().split())
    #A = list(filter(lambda a: a <= k,A)) # remove useless items (no need)
    A = list(set(A)) # remove the duplicates

    M = [0]
    for x in range(1,k+1):
        possibilities = [M[x-1]]
        for w in A:
            if w <= x:
                possibilities.append(M[x-w]+w)
        M.append(max(possibilities))

    print(M[k])
