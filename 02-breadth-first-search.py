#!/usr/bin/python3 -tt
'''------------------------------------- >Last Modified on Fri, 27 May 2016< '''
'''
Breadth First Search: Shortest Reach
https://www.hackerrank.com/challenges/bfsshortreach
Worst case time performance: O(|V|+|E|)

The problem: find a shortest distance between every vertex and a root in a 
given graph. The graph is undirected and each edge has a (positive) constant 
length.

Breadth-first search ( https://en.wikipedia.org/wiki/Breadth-first_search )
is an algorithm to find reachable vertices from a specified vertex (root) in
a graph. This code is an honest implementation of BFS.

The idea of the algorithm is very simple:

1. Let V_0 be a singleton of the root.
2. V_1 be the set of vertices which can be reached from the root in one edge.
3. V_2 be the set of vertices which can be reached from a vertex in V_1 and 
   are not included in V_0 cup V_1
   ....
k+1. V_k be the set of vertices which can be reached from a vertex in V_{k-1}
     and are not included in V_0 cup ... cup V_{k-1}.

If V_k is empty, then the algorithm stops. Instead of V_i's we use a list Q.
Namely we start with a singleton Q = [root] and we examine vertices in Q.
Remove the first vertex v from Q (the queue), add not examined vertices which
can be reached from v.

The distance is stored in the vertex variable (a dict in this code).
'''

from collections import defaultdict

for _ in range(int(input())):
    n,m = map(int,input().split()) # number of nodes/edges

    edges = defaultdict(list) # dict: vertex -> list of adjacent vertices
    for _ in range(m):
        e0,e1 = map(int,input().split())
        edges[e0].append(e1)
        edges[e1].append(e0) # to make the data symmetric (undirected graph)

    root = int(input()) # the root vertex

    all_nodes = [{'index':i+1,'distance':-1,'parent':None} for i in range(n)]
    # Each vertex is represented as a dict.
    # index : the node index (1 to n)
    # distance : the distance from the root. The initial value is -1
    # parent : the previous vertex in a short path from the root

    all_nodes[root-1]['distance'] = 0 # dist(root,root) = 0
    Q = [all_nodes[root-1]] # the singleton of the root 

    while len(Q) > 0:
        current = Q[0]
        Q = [] if len(Q) == 1 else Q[1:]

        for edge in edges[current['index']]:
            next_node = all_nodes[edge-1]
            if next_node['distance'] == -1:
                next_node['distance'] = current['distance']+6
                next_node['parent'] = current['index']
                Q.append(next_node)

    distances = [node['distance'] for node in all_nodes]
    del distances[root-1] # remove the root
    print(' '.join(map(str,distances)))
