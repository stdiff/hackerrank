#!/usr/bin/python3 -tt
'''------------------------------------- >Last Modified on Thu, 02 Jun 2016< '''
'''
Dijkstra: Shortest Reach 2
https://www.hackerrank.com/challenges/dijkstrashortreach

The problem: a undirected graph with weights (lengths) is given. Find the 
shortest distance from a root to every vertex.

Dijkstra's algorithsm ( https://en.wikipedia.org/wiki/Dijkstra's_algorithm )
can be applied to such a problem. See also 
http://www.geeksforgeeks.org/greedy-algorithms-set-6-dijkstras-shortest-path-algorithm/

The idea of the algorithm is following:

1. Consider the queue Q of all vertices. Each vertex contains the following data:
   index (vertex id), distance from the root (Inf is the initial value) and the
   parent vertex (None is the initial value).
2. Set the distance of the root as 0
3. (a) Pick and remove one of the vertices in Q with the *minimum* distance.
       (The root is chosen at the first time.)
   (b) Each adjacent vertex u of the chosen vertex v will be updated, if u
       is still in the queue Q and a shorter path to u is found.
   (c) If Q is not empty, go to (a)

The reason for the minimal distance in (a) is that the (chosen) vertex is not 
going to be updated any more. Namely the queue Q is the queue of vertices which
could be updated later. We may make use of minimum heap structure for Q.

The time complexity of the algorithm is O(|V^2|) (with a matrix representation).
If we use a min-priority queue for Q, then the time complexty is reduced to 
O(E+VlogV) (with Fibonacci heap).

In this code Python's heapq ( https://docs.python.org/3/library/heapq.html )
is used, but unfortunately this code is not so fast that it gives the answer
of the last test case on HackerRank in 10 seconds. (On my PC I get the answer
in around 17 seconds.)
'''

M = 10**9

from collections import defaultdict
import heapq

def decreaseKey(queue,old,new):
    '''
    This function corresponds decrease_priority() in Wikipedia. 
    For the implimentation I follow GeeksForGeeks. (See the above comment.)
    '''
    pos = 0
    for i in range(len(queue)):
        if queue[i] == old:
            pos = i
            break

    queue[pos][0] = new[0] # update the value

    # swap until the list is heapified
    while pos and queue[pos][0] < queue[(pos-1)//2][0]:
        old_parent = queue[(pos-1)//2]
        old_child = queue[pos]

        queue[(pos-1)//2] = old_child # parent
        queue[pos] = old_parent # child
        pos = (pos-1)//2

    return queue

my_answer = []
for i in range(int(input())):
    n,m = map(int,input().split()) # number of vertices/edges

    outgoing = defaultdict(set) # vertex -> set of adacent vertices
    weights = defaultdict(lambda : defaultdict(int)) # weights[v0][v1] -> w

    for _ in range(m):
        s,t,w = map(int,input().split())
        outgoing[s].add(t)
        outgoing[t].add(s) # symmetry (undirected graph)
        if weights[s][t] == 0 or weights[s][t] > w:
            # if the weight is not defined (0) or a smaller weight is found,
            # then the weight will be updated
            weights[s][t] = w
            weights[t][s] = w # symmetry (undirected graph)
    root = int(input()) # the root

    '''
    Every vertex is represented as a list v (because of heapq of Python).
    v[0]=dist : the distance from the root. The default value is w_max (Inf)
    v[1]=index: the vertex index starting from 1
    '''
    all_vertices = []
    Q = [] # queue of vertices (priority queue)
    for i in range(n):
        ind = i+1
        vertex = [0,ind] if ind == root else [M,ind] # v[0]: dist, v[1]: ind
        heapq.heappush(Q,vertex) # Q.add_with_priority()
        all_vertices.append(vertex)

    while Q:
        u = heapq.heappop(Q) # Q.extract_min()

        ### update the adjacent vertices which are still in the queue
        for neighbour_index in outgoing[u[1]]:
            neighbour = None
            for node in Q:
                if node[1] == neighbour_index:
                    neighbour = all_vertices[neighbour_index-1]
                    break

            if neighbour:
                alt = u[0] + weights[u[1]][neighbour_index]
                if alt < neighbour[0]:
                    new_neighbour = [alt,neighbour_index]
                    Q = decreaseKey(Q,neighbour,new_neighbour) # Q_decrease_priority()
                    all_vertices[neighbour_index-1][0] = alt

    shortest_paths = [node[0] if node[0] != M else -1 for node in all_vertices]
    del shortest_paths[root-1]
    my_answer.append(shortest_paths)

for shortest_paths in my_answer:
    print(' '.join(map(str,shortest_paths)))


'''
The following code checks the calculated solution.
'''
def check_answer(output_file,my_answer):
    answer = []
    with open(output_file,'r') as fo:
        for line in fo:
            answer.append([int(x) for x in line.rstrip().split()])
    print('-------------answer check---------------')
    for i in range(len(answer)):
        print('TESTCASE:',i)
        if answer[i] != my_answer[i]:
            print('TRUE:',[str(x) for x in answer[i]])
            print('MINE:',[str(x) for x in my_answer[i]])
#check_answer('output-6.txt',my_answer)
