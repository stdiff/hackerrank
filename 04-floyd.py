#!/usr/bin/python3 -tt
'''------------------------------------- >Last Modified on Sun, 29 May 2016< '''
'''
Floyd : City of Blinding Lights
https://www.hackerrank.com/challenges/floyd-city-of-blinding-lights

The problem: a directed graph with weights (length) is given. Find the 
shortest distances between given pairs of two pairs.

This is a typical situation which Floyd-Warshall algorithm is applied to.
https://de.wikipedia.org/wiki/Algorithmus_von_Floyd_und_Warshall
http://www.geeksforgeeks.org/dynamic-programming-set-16-floyd-warshall-algorithm/

The algorithm can be implemented with dynamic programming: let M[s,t,k] be the
shortest distance from s to t through the vertices {1,...,k}. If the last index
of the vertex is n, then M[s,t,n] is what we want.

    m(s,t,0) = w(s,t) (if there is an edge between s and t)
             = Inf    (else)
    m(s,t,k+1) = min( m(s,t,k),   m(s,k+1,k)+m(k+1,t,k) )
                      without k+1           through k+1

The computation is very simple, but the time complexity is O(V^3), i.e. it is 
independent of the number of the edges while the algorithm is very slow and 
requires huge memory space if there is a lot of vertices.

In the test cases 3-5 there are 400 vertices. This code, honest implementation
of Floyd-Warshall algorithm, does not working for the test cases unfortunately.

To solve all test cases, we should implement Dijkstra's algorithm instead.
'''

from collections import defaultdict

n,m = map(int,input().split()) # number of vertices/edges

edges = defaultdict(lambda : defaultdict(int)) # [s][t] -> length of the edge
for _ in range(m):
    s,t,w = map(int,input().split())
    edges[s][t] = w # directed graph

length = defaultdict(lambda : defaultdict(lambda: defaultdict(int)))
for s in range(1,n+1):     # s : source
    for t in range(1,n+1): # t : target
        d = length[s][t]
        if s==t: # source = target
            d[1] = 0
        elif edges[s][t] != 0: # there is an edge from s to t
            d[1] = edges[s][t]
        else:
            d[1] = 10**9

for k in range(2,n+1):
    for s in range(1,n+1):
        for t in range(1,n+1):
            length[s][t][k] = min([length[s][t][k-1],
                                   length[s][k][k-1]+length[k][t][k-1]])

for _ in range(int(input())): # read questions and solve them
    s,t = map(int,input().split())
    if length[s][t][n] == 10**9:
        print(-1)
    else:
        print(length[s][t][n])
