#!/usr/bin/python3 -tt
'''------------------------------------- >Last Modified on Sun, 29 May 2016< '''
'''
Prim's (MST) : Special Subtree
https://www.hackerrank.com/challenges/primsmstsub

The problem: find a minimum spanning tree T of a given weighted (undirected) 
graph G. Here a minimum spanning tree T of G is a subgraph of G such that

1. T is a tree,
2. the set of vertices of T agrees with one of G, and
3. the sum of the weights in the tree is minimum.

As a note a tree is a undirected graph in which any two vertices are connected
by exactly one path.

The DJP algorithm (a.k.a Prim’s algorithm) is one of the algorithms to find a
minimum spanning tree. The idea is very easy.

1. We start with two sets: Tree (vertices in the tree) and Q (queue of vertices)
2. Initialization: every vertex has a "value". The initial value is 0 if the 
   vertex is the root, else Inf.
3. While Q is not empty (or Tree does not contain all vertices), do following
   (a) Pick and remove a vertex, say u, in Q with the smallest value.
   (b) Add u to the set Tree.
   (c) Update the values of adjacent vertices of u in Q. Namely if the value 
       of an adjacent vertex v is smaller than the weight w of the edge between
       u and v, then the value of v becomes the w

Note that we assume that the given graph G is connected. If we want to deal with
a non-connected graph, then we have to look at the value of u in (a) whether the
value of u is Inf. In such a case we also note that the tree which we obtained 
is not a spanning tree.

Since we choose a vertex of the minimum value in 3(a), we can make use of a
priority queue as a structure of Q. In this code we just sort Q, because it is
enough to solve all test cases of the problem.

You should also consult
http://www.geeksforgeeks.org/greedy-algorithms-set-5-prims-mst-for-adjacency-list-representation/
for details of the algorithm.
'''

M = 10**9

from collections import defaultdict

n,m = map(int,input().split()) # number of vertices/edges

weights = defaultdict(lambda : defaultdict(int))
outgoing = defaultdict(set)
for _ in range(m):
    s,t,w = map(int,input().split())
    weights[s][t] = w
    weights[t][s] = w
    outgoing[s].add(t)
    outgoing[t].add(s)
    
root = int(input()) # the index of the root of the spanning tree

Tree = set()
all_vertices = [{'index':i+1,'value':M,'parent':None} for i in range(n)]
all_vertices[root-1]['value'] = 0
all_vertices[root-1]['parent'] = root

Q = all_vertices.copy()

while len(Tree) < n:
    Q.sort(key=lambda vertex: vertex['value'])
    u = Q[0] # pick a vertex with the smallest value
    s = u['index'] # the index of the picked vertex (source)
    del Q[0] # delete it from the vertex queue
    Tree.add(u['index']) # add the index of u to the tree

    for t in outgoing[s]:
        v = all_vertices[t-1] # adjacent vertex
        if t not in Tree and weights[s][t] < v['value']:
            v['value'] = weights[s][t]
            v['parent'] = s
    #print(Tree)

# print the sum of weiths in the minimum spanning tree
print(sum([vertex['value'] for vertex in all_vertices]))
