#!/usr/bin/python3 -tt
'''------------------------------------- >Last Modified on Sun, 05 Jun 2016< '''
'''
PackMan - DFS
https://www.hackerrank.com/challenges/pacman-dfs

The problem: find a path to the goal in a maze by using DFS.

Here DFS is Depth-first search and we have to find a path by using only DFS.
The problem description is very confusing, but we can see a process of DFS if 
we submit the correct solution. You should consult the discussion forum, but 
note that the problem is slightly changed before. Namely old posts might also 
be confusing.

An example of a maze is following:

%%%%%%%%%
%-------%     P : PackMan (start)
%-%%-%%-%     . : food (goal)
%---P---%     % : wall (you can not go through)
%-%%-%%-%     - : road
%.------%
%%%%%%%%%

First we regard the maze as a graph in a following way: vertices are all places
which PackMan stands. Edges are pairs of vertices which are next to each other.

In DFS we use a stack of vertices. The list "expolored" consists of 
vertices which is picked from the stack in the algorithm. Namely an explored
vertex is a vertex examined in the algorithm until the goal is examined.

The DFS can be applied to a graph whose edges have the same weight (length).
Its idea is very simple: 

1) S is a stack of vertices which are going to be examined.
2) pick the last vertex from S and examine it as follows.
   a) If there is an adjacent vertex is not visited, then set the parent vertex
      of the adjacent vertex as the current vertex. 
   b) Add the adjacent vertices to S unless they already belong to S

This code is an honest implementation of the DFS algorithsm. 
( https://en.wikipedia.org/wiki/Depth-first_search )

By this algorithm the following numbered vertices are explored until the goal is
examined. 

%% %% %% %% %% %% %% %% %% 
%% 16 15 14 13 12 11 10 %% 
%% 17 %% %%    %% %% 09 %% 
%% 18       00    08 07 %% 
%% 19 %% %% 01 %% %% 06 %% 
%% 20       02 03 04 05 %% 
%% %% %% %% %% %% %% %% %% 

The number is the order of examination. 

As the result we get the following path from the start to the goal. 

%% %% %% %% %% %% %% %% %% 
%% 15 14 13 12 11 10 09 %% 
%% 16 %% %%    %% %% 08 %% 
%% 17       00       07 %% 
%% 18 %% %% 01 %% %% 06 %% 
%% 19       02 03 04 05 %% 
%% %% %% %% %% %% %% %% %% 

In the next problem we solve the same problem by using BFS.
'''

from collections import defaultdict # for non-existing key of dict

def show_maze(n,p,grid):
    '''
    Show the original maze (without the start or the goal).
    '''
    print(' '.join(['%3d' % j for j in range(p)]))

    for i in range(n):
        for j in range(p):
            if grid[i][j] != '%':
                print('   ',end=' ')
            else:
                print('===',end=' ')
        print('%3d' % i)

def show_path(n,p,path,grid,all_vertices):
    '''
    This can be used to visualise both the search process and the found path.
    '''
    for i in range(n):
        for j in range(p):
            if (i,j) in path:
                step = path.index((i,j))
                print('%03d' % step,end=' ')
            else:
                if grid[i][j] == '%':
                    print('===',end=' ')
                else:
                    print('   ',end=' ')
        print()

def print_answer(explored,path):
    '''
    This function prints out the answer in a required format. 
    '''
    print(len(explored))
    for x in explored:
        print('%s %s' % x)
    print(len(path)-1)
    for x in path:
        print('%s %s' % x)

r,c = map(int,input().split()) # the initial position of PackMan
food = tuple(map(int,input().split())) # the position of the food
n,p = map(int,input().split()) # the dimension of the grid

grid = [list(input().rstrip()) for _ in range(n)]

all_vertices = {} # (r,c) -> vertex information
outgoing = defaultdict(list) # (r,c) -> [(r0,c0),...]

for i in range(n):
    for j in range(p):
        if grid[i][j] != '%':
            vertex = {'coord':(i,j),'visited':False,'parent':None}
            all_vertices[(i,j)] = vertex
            ### Note that the order of vertices which are pushed into the stack
            ### is specified in the problem statement.
            if i-1 >= 0 and grid[i-1][j] != '%': # UP
                outgoing[(i,j)].append((i-1,j))
            if j-1 >= 0 and grid[i][j-1] != '%': # LEFT
                outgoing[(i,j)].append((i,j-1))
            if j+1 <= p-1 and grid[i][j+1] != '%': # RIGHT
                outgoing[(i,j)].append((i,j+1))
            if i+1 <= n-1 and grid[i+1][j] != '%': # DOWN 
                outgoing[(i,j)].append((i+1,j))

root = all_vertices[(r,c)]
root['parent'] = (r,c) # initialization
S = [root] # stack of vertices 
explored = [] # list of examined vertices

while len(S) > 0:
    u = S.pop()

    if u['visited'] == False:
        u['visited'] = True
        explored.append(u['coord'])

        if u['coord'] == food: # if the goal is found, stop the loop
            break

        for coord in outgoing[u['coord']]:
            v = all_vertices[coord]

            if v['parent'] == None:
                v['parent'] = u['coord']
            if v not in S:
                S.append(v)

### find a path to the goal, looking at the parent vertex from the goal
path = [food]
coord = food
while coord != (r,c):
    coord = all_vertices[coord]['parent']
    path.append(coord)
path = path[::-1]

print('------------------------------ explored')
show_path(n,p,explored,grid,all_vertices)
print('---------------------------------- path')
show_path(n,p,path,grid,all_vertices)
print('---------------------------------- maze')
show_maze(n,p,grid)

#print_answer(explored,path)

