#!/usr/bin/python3 -tt
'''------------------------------------- >Last Modified on Sun, 05 Jun 2016< '''
'''
PackMan - BFS
https://www.hackerrank.com/challenges/pacman-bfs

The problem: find a path to the goal in a maze by using BFS.

This is a BFS version of the previous code "PackMan - DFS". The aim of these 
problem is to visualise a search process of BFS (and compare it with one of DFS).

Again here is an example of a maze:

%%%%%%%%%
%-------%     P : PackMan (start)
%-%%-%%-%     . : food (goal)
%---P---%     % : wall (you can not go through)
%-%%-%%-%     - : road
%.------%
%%%%%%%%%

We regard the maze as a graph as before. The details of BFS can be found in 
the code "02-breadth-first-search.py".

Here is a search process, namely the examined vertices. To find a path to the 
goal we need to examine almost all vertices.

%% %% %% %% %% %% %% %% %% 
%% 23 15 09 05 10 16 24 %% 
%% 17 %% %% 01 %% %% 19 %% 
%% 11 06 02 00 03 07 12 %% 
%% 18 %% %% 04 %% %% 20 %% 
%% 25 21 13 08 14 22    %% 
%% %% %% %% %% %% %% %% %% 

The following is the (shortest) path which we find with BFS.

%% %% %% %% %% %% %% %% %% 
%%                      %% 
%%    %% %%    %% %%    %% 
%% 03 02 01 00          %% 
%% 04 %% %%    %% %%    %% 
%% 05                   %% 
%% %% %% %% %% %% %% %% %% 

The biggest difference between BFS and DFS is: BFS gives one of shortest paths
to the goal while DFS will give a path the minite any path to the goal has been
found. In other words we have to examine a lot of vertices in BFS to guarantee
that the given path is shortest, but in DFS we do not need to see (relatively)
many vertices. This DFS is used to find a connected component of a graph.
'''

from collections import defaultdict # for non-existing key of dict

def show_maze(n,p,grid):
    '''
    Show the original maze (without the start or the goal).
    '''
    print(' '.join(['%3d' % j for j in range(p)]))

    for i in range(n):
        for j in range(p):
            if grid[i][j] != '%':
                print('   ',end=' ')
            else:
                print('===',end=' ')
        print('%3d' % i)

def show_path(n,p,path,grid,all_vertices):
    '''
    This can be used to visualise both the search process and the found path.
    '''
    for i in range(n):
        for j in range(p):
            if (i,j) in path:
                step = path.index((i,j))
                print('%03d' % step,end=' ')
            else:
                if grid[i][j] == '%':
                    print('===',end=' ')
                else:
                    print('   ',end=' ')
        print()

def print_answer(explored,path):
    '''
    This function prints out the answer in a required format. 
    '''
    print(len(explored))
    for x in explored:
        print('%s %s' % x)
    print(len(path)-1)
    for x in path:
        print('%s %s' % x)

r,c = map(int,input().split()) # the initial position of PackMan
food = tuple(map(int,input().split())) # the position of the food
n,p = map(int,input().split()) # the dimension of the grid

grid = [list(input().rstrip()) for _ in range(n)]

all_vertices = {} # (r,c) -> vertex information
outgoing = defaultdict(list) # (r,c) -> [(r0,c0),...]

for i in range(n):
    for j in range(p):
        if grid[i][j] != '%':
            vertex = {'coord':(i,j),'distance':-1,'parent':None}
            all_vertices[(i,j)] = vertex
            ### Note that the order of vertices which are pushed into the queue
            ### is specified in the problem statement.
            if i-1 >= 0 and grid[i-1][j] != '%': # UP
                outgoing[(i,j)].append((i-1,j))
            if j-1 >= 0 and grid[i][j-1] != '%': # LEFT
                outgoing[(i,j)].append((i,j-1))
            if j+1 <= p-1 and grid[i][j+1] != '%': # RIGHT
                outgoing[(i,j)].append((i,j+1))
            if i+1 <= n-1 and grid[i+1][j] != '%': # DOWN 
                outgoing[(i,j)].append((i+1,j))

root = all_vertices[(r,c)]
root['parent'] = (r,c) # initialisation 
root['distance'] = 0
Q = [root] # queue of vertices
explored = [] # list of examined vertices

while len(Q) > 0:
    u = Q.pop(0) # shift in Perl
    explored.append(u['coord'])

    if u['coord'] == food: # if the goal is found, stop the loop
        break
    
    for coord in outgoing[u['coord']]:
        v = all_vertices[coord]

        if v['distance'] == -1:
            v['distance'] = u['distance']+1
            v['parent'] = u['coord']
            Q.append(v)

### find a path to the goal, looking at the parent vertex from the goal
path = [food]
coord = food
while coord != (r,c):
    coord = all_vertices[coord]['parent']
    path.append(coord)
path = path[::-1]

print('------------------------------ explored')
show_path(n,p,explored,grid,all_vertices)
print('---------------------------------- path')
show_path(n,p,path,grid,all_vertices)
print('---------------------------------- maze')
show_maze(n,p,grid)

#print_answer(explored,path)
        
