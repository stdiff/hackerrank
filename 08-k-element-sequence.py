#!/usr/bin/python3 -tt
'''------------------------------------- >Last Modified on Fri, 03 Jun 2016< '''
'''
K-element Sequences
https://www.hackerrank.com/contests/infinitum15/challenges/k-element-sequences

The problem: compute the binomial coefficient

       (n-1)!
    ------------
    (n-k)!(k-1)!

in mod M. Here M is a prime number 10^9+7.

This belongs to the contest "Ad Infinitum 15 - Math Programming Contest". But 
the difficulty does not come from mathematics. The actual problem is that 1000
pair of n and k are given. This means that we need to efficiently compute the
given 1000 binomial coefficients (in 10 seconds). If we write a code without
any idea, then the computation never ends.

Before discussing efficient computation, we note that every computation is in
the finite field F_M (=Z/MZ). Thus 1/A is given by A^{M-2}. This is a direct
conclusion of Fermat's little theorem. Therefore

    a/(b*c) = a * b^{M-2} * c^{M-2}

holds in F_M. This is very important for the computation, because the quotient
in Z (or Q) and one in F_M are quite different. For example

    4 = 12/3 = 1/3

holds in F_11. But 1/3 is not 0.333... in F_11. In order to compute a quotient
in a finite field, we need to compute an inverse element in a suitable way.

To reduce the computation we may make use of dynamical programming: we have the
identities

    (n 0) = (n n) = 1, and 
    (n k) = (n-1 k-1) + (n-1 k).

Here (n k) is a binomial coefficient. Thus we may calculate all binomial 
coefficients in advance. But this does not work for the problem because this 
DP requires to store around (n^2)/2 numbers and n could be 2*10^6-1.

If we calculate in advance functorials which we need, then we can pass all
test cases. This largely reduces computation time and requires to store at
most 2*10^6-1 numbers.
'''

M = 10**9+7

def bc(n,k):
    import math
    #from numpy import math # NumPy is not available on hackerRank
    '''
    This compute n!/k!(n-k)! directly.
    '''
    a = math.factorial(n) % M            # n! in mod M
    b = pow(math.factorial(k)%M,M-2,M)   # k!^{-1} in mod M
    c = pow(math.factorial(n-k)%M,M-2,M) # (n-k)!^{-1} in mod M
    return (a*b*c) % M

pairs = []
n_max = 1

for _ in range(int(input())):
    n,k = map(int,input().split())
    pairs.append([n,k])
    if n > n_max:
        n_max = n

### Compute all factorials which we need.
factorial = [1]
for n in range(1,n_max):
    factorial.append( (factorial[n-1]*n) % M )

for pair in pairs:
    n = pair[0]-1
    k = pair[1]-1
    a = factorial[n] % M            # n! in mod M
    b = pow(factorial[k]%M,M-2,M)   # k!^{-1} in mod M
    c = pow(factorial[n-k]%M,M-2,M) # (n-k)!^{-1} in mod M
    answer = (a*b*c)%M
    print(answer)
    # print(bc(n,k) == answer) # check if the computation is correct.
