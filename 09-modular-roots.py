#!/usr/bin/python3 -tt
'''------------------------------------- >Last Modified on Sun, 05 Jun 2016< '''
'''
Modular Roots
https://www.hackerrank.com/contests/infinitum15/challenges/modular-roots

The problem: find roots of the equation x^k = n in Z/pZ. Here p is prime.

This problem belongs to the contest "Ad Infinitum 15 - Math Programming Contest".
But the difficulty does not come from mathematics as well. It is not enough to 
implement the formula of the roots.

# The formula of the roots

Before going to details, we describe the formula of the roots. Note that we 
are working on a finite field F_p, because p is a prime number. Thus if n = 0,
then the only solution is 0. Thus we assume that n != 0.

Using a primitive root pr (i.e. a generator of the multiplicative of F_p), we 
can reduce the equation to a linear equation in a following way: there are y and
l such that x=pr^y and n=pr^l hold. Then the given equation can be written as 

(1)    pr^{ky} = pr^l in Z/pZ.

Since pr is a primitive root, the equation is equivalent to 

(2)    ky = l in Z/(p-1)Z
    
If k = 0, then there is no solution (l=0) or y=0,..,p-2 (l != 0).

We assume that k != 0. k is not invertible in Z/(p-1)Z in general. Thus we make
it invertible by using the gcd g of k and p-1. (The following proceduce can be 
applied even though k is already invertible in Z/(p-1).)

If g != 1 and not g|l, there is no solution, thus we assume that g|l. Let k',
l' and m be k/g, l/g and (p-1)/g, respectively. Then the equation (2) is
equivalent to 

(3)    k' y = l' in Z/mZ

Now k' is invertible, y is of form y = l'/k' + mZ in Z/(p-1)Z. More explicitly

(4)    y = l'/k' + am in Z/(p-1)Z for a=0,..,g-1.

Note that our discussion assume the existance of a primitive root.

# Tipps 

As we mentionded above, an honest implimentation is not enoughly fast, when 
there are especially lots of test cases. 

- Use pow(a,k,n) to compute a^k in Z/nZ.
- We should also avoid to use pow(a,k,n) when computing many terms of a 
  geometric sequence. Use its recursive relation instead.
- We should initialise a list instead of using defaultdict, when the maximum
  lengh of the list is explicitly given.
- Avoid to use L.index() for a list L, create a dictionaly Linv such that
  Linv[L[i]] == i if L is a permutation of len(l) letters.

'''

from functools import reduce
from collections import defaultdict

def is_prime(n:int):
    import math
    '''
    Check in a naive way whether a given number is prime.
    '''
    n = abs(n)
    if n <= 1:
        return False
    else:
        for m in range(2,int(math.sqrt(n))+1):
            if n % m == 0:
                return False
        return True


def prime_factors(m:int):
    '''
    Execute the prime factorisation in a naive way. (This might be slow.)
    This function return a defaultdict. If m = p1^k1 * p2^k2 * ..., then 
        p1 => k1, p2 => k2, ...
    '''
    factors = defaultdict(int) 

    while m % 2 == 0:
        factors[2] += 1
        m = m // 2

    p = 3
    while p*p <= m:
        # p is just an odd number, but there is no problem.
        while m % p == 0:
            factors[p] += 1
            m = m//p
        p += 2

    return factors


def primitive_root(p:int):
    '''
    WARNING: we assume that p is prime.

    Find a "smallest" primitive root of Z/pZ. If g is a primitive root,
    then for any prime number q such that q|p-1 the power g^{(p-1)/q} is
    not 1. We check one-by-one if this statement holds.

    This method is probabily better than brute force as long as the prime
    decomposition of p can be quickly calculated.

    To extend this function for a nonprime number, we need to compute
    Euler's phi of p.
    '''

    if p == 2:
        return 1
    else:
        factors = prime_factors(p-1)

        pr = 1
        while pr < p:
            mks = [pow(pr,(p-1)//q,p) for q in factors.keys()]
            if all([mk != 1 for mk in mks]):
                break
            else:
                pr += 1
        return pr

def eulers_phi(n:int):
    '''
    Euler's phi function: phi(n) = # of multiplicative group of Z/nZ
    '''
    if n == 1:
        return 1
    elif n >= 2:
        factors = prime_factors(n)
        return reduce(lambda x,y: x*y,
                      [q**(factors[q]-1)*(q-1) for q in factors.keys()])
    else:
        return None

def egcd(a,b):
    '''
    extended Euclidean algorithm
    https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm

    Given pair of two (positive) integers a and b, this function compute 
    gratest common divisor g of a and b and one of solutions of the equation
        ax+by=g
    This function returns tuple (g, x0, y0).
    '''
    s,t,r = 0,1,b
    s_prev,t_prev,r_prev = 1,0,a
    while r != 0:
        q = r_prev // r
        s_prev, s = s, s_prev - q*s
        t_prev, t = t, t_prev - q*t
        r_prev, r = r, r_prev - q*r
    return r_prev, s_prev, t_prev # (g, x0, y0)
    
####################################################################### __main__

p,Q = map(int,input().split()) # mod p and number of test cases 
### Note that we assume in this code that p is a prime number.

pr = primitive_root(p)

### slightly better than using defaultdict
prpow = [None]*p # k -> pr^k
logpr = [None]*p # pr^k -> k

prpow[0] = 1

for k in range(1,p):
    ak = (prpow[k-1]*pr) % p
    prpow[k] = ak
    logpr[ak] = k

for _ in range(Q):
    ### solve x^k = n in mod p
    k,n = map(int,input().split())

    k = k % (p-1) # Applying Fermat's little theorem
    n = n % p

    roots = []
    if n == 0:
        roots = [0] # because there is no zero divisor.
    else:
        if k == 0:
            if n == 1: # x^k == 1
                roots = list(range(1,p)) # solution is x != 0.
        else:
            ### Solve ky = l for y in mod p-1.
            #l = mgp.index(n)+1 
            l = logpr[n] # 
            g,a,b = egcd(k,p-1) # a*k + b*(p-1) = g holds.

            if p == 2:
                roots = [n]
            elif g == 1 or l % g == 0:
                ### Solve (k/g) y = (l/g) in mod (p-1)/g
                ### Since gcd(k/g,(p-1)/g)=1, (k/g){-1} exists in mod (p-1)/g
                k, l, m = k//g, l//g, (p-1)//g

                # a*k + b*m = 1 holds. Therefore a is 1/k in mod m
                c = (l*a) % m
                x0 = pow(pr,c,p) # pr^c mod p
                d =  pow(pr,m,p) # pr^m mod p

                # solution: x = pr^{c+am} = x0*d^a (a=0,..,g-1)
                roots.append(x0)
                for h in range(1,g):
                    x0 = (x0*d) % p # use the recursive relation instead of pow()
                    roots.append(x0)
                roots.sort()
                
    if roots:
        print(" ".join(map(str,roots)))
    else:
        print('NONE')
