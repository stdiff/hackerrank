# My notes on algorithms 

This repository is just for my notes and source codes for problems at [HackerRank](https://www.hackerrank.com/domains).

- The comments and notes can be found in the top of a source code.
- All codes are written in Python (or Perl).
- Each code is written to solve a question at HackerRank. But this does not mean that the maximum score can be obtain with the code.
- A person who will submit my code at HackerRank is definitely crazy. HackerRank is not the place to check the performance of codes which someone wrote.
- My explanation of mathematics might be readable only by experts. 


## Motivation

There is a time limit at HackerRank: **[a python code must give the answer in 10 seconds](https://www.hackerrank.com/environment)**. Because of the limit it is not a good idea to use Python at HackerRank, when we try to write a code including a huge number of computation.

In other words: **we need a substantial idea to reduce a time complexity.**

The only thing you can find here is my codes which include my notes on time complexity and algorithms.


